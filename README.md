# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Neste projeto, o programa inicia-se com uma tela de login onde os campos Funcionario e Senha devem ser preenchidos por "funcionario" e "12345" respectivamente.

Após logar, o funcionario terá capacidade de fazer um pedido, cadastrar produto ao estoque ou cadastrar um cliente.

Para cadastrar um cliente é necessario informar: Nome completo, Telefone, Endereco.

Para cadastrar um produto é necessario informar: ID, Nome, Tipo, Quantidade e o Preco.

O pedido do cliente esta incompleto.
