package controle;

import java.util.ArrayList;

import modelo.Cliente;

/**
 *
 * @author ziegler
 */
public class DadosCliente {
    
    
    private final ArrayList<Cliente> listaCliente;
   
    
    public DadosCliente() {
        this.listaCliente = new ArrayList<>();
    }
    
    public ArrayList<Cliente> getListaCliente() {
        return listaCliente;
    } 
    
    public void cadastraCliente(Cliente cliente) {
        listaCliente.add(cliente);        
    }
    
    public void removerCliente(Cliente cliente) {
        listaCliente.remove(cliente);
    }
    
    public Cliente pesquisar(String nome) {
        for (Cliente c: listaCliente) {
            if (c.getNome().equalsIgnoreCase(nome)) return c;
        }
        return null;
    }
    
}