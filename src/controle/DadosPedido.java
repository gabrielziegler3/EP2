package controle;

import java.util.ArrayList;

import modelo.Pedido;

/**
 *
 * @author ziegler
 */
public class DadosPedido {
    
    
    private final ArrayList<Pedido> listaPedido;
   
    
    public DadosPedido() {
        this.listaPedido = new ArrayList<>();
    }
    
    /**
     *
     * @return
     */
    public ArrayList<Pedido> getListaPedido() {
        return listaPedido;
    } 
    
    public void cadastraPedido(Pedido pedido) {
        listaPedido.add(pedido);        
    }
    
    public void removerPedido(Pedido pedido) {
        listaPedido.remove(pedido);
    }
    
    public Pedido pesquisar(String nome) {
        for (Pedido c: listaPedido) {
            if (c.getProduto().equalsIgnoreCase(nome)) return c;
        }
        return null;
    }
    
}