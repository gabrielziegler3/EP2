package controle;

import java.util.ArrayList;

import modelo.Produto;

public class DadosProduto {
    
    
    private final ArrayList<Produto> listaProdutos;
    
    public DadosProduto() {
        this.listaProdutos = new ArrayList<>();
    }
    
    public ArrayList<Produto> getListaProduto() {
        return listaProdutos;
    }
    
    
    public void cadastraProduto (Produto produto) {
        listaProdutos.add(produto);
    }
 
    public void removerProduto(Produto produto) {
        listaProdutos.remove(produto);
    }
    
    public Produto pesquisar(String nomeProduto) {
        for (Produto p: listaProdutos) {
            if (p.getTipo().equalsIgnoreCase(nomeProduto)) return p;
        }
        return null;
    }
}