/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visao;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import modelo.Pedido;
import controle.DadosPedido;

/**
 *
 * @author ziegler
 */
public class FazerPedido extends javax.swing.JFrame {
    
    Pedido pedido = new Pedido();
    
    Pedido sanduiche = new Pedido("Sanduiche", "1", "10");
    
    Pedido batatas = new Pedido("Batatas", "1", "5");
    
    Pedido salada = new Pedido("Salada", "1", "7");

    Pedido agua = new Pedido("Agua", "1", "2");
    
    Pedido aguaDeCoco = new Pedido("aguaDeCoco", "1", "3");
    
    Pedido cerveja = new Pedido("Cerveja", "1", "3");

    
    /**
     * Creates new form Pedido
     */
    public FazerPedido() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelPedidos = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButtonFritas = new javax.swing.JButton();
        jButtonSalada = new javax.swing.JButton();
        jButtonAgua = new javax.swing.JButton();
        jButtonCerveja = new javax.swing.JButton();
        jButtonSanduiche = new javax.swing.JButton();
        jButtonCoco = new javax.swing.JButton();
        jLabelBatata = new javax.swing.JLabel();
        jLabelSalada = new javax.swing.JLabel();
        jLabelAgua = new javax.swing.JLabel();
        jLabelCoco = new javax.swing.JLabel();
        jLabelCerveja = new javax.swing.JLabel();
        jLabelSanduiche = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaPedido = new javax.swing.JTable();
        jLabelFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        jLabelPedidos.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        jLabelPedidos.setText("Pedidos");
        getContentPane().add(jLabelPedidos);
        jLabelPedidos.setBounds(10, 0, 110, 40);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        jButtonFritas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fries.png"))); // NOI18N
        jPanel1.add(jButtonFritas);
        jButtonFritas.setBounds(200, 50, 140, 130);

        jButtonSalada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salad.png"))); // NOI18N
        jPanel1.add(jButtonSalada);
        jButtonSalada.setBounds(360, 50, 140, 130);

        jButtonAgua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/agua.png"))); // NOI18N
        jPanel1.add(jButtonAgua);
        jButtonAgua.setBounds(40, 230, 140, 130);

        jButtonCerveja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cerveja.png"))); // NOI18N
        jPanel1.add(jButtonCerveja);
        jButtonCerveja.setBounds(360, 230, 140, 130);

        jButtonSanduiche.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/hamburger-icon.png"))); // NOI18N
        jButtonSanduiche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSanduicheActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonSanduiche);
        jButtonSanduiche.setBounds(40, 50, 140, 130);

        jButtonCoco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/aguadecoco.png"))); // NOI18N
        jPanel1.add(jButtonCoco);
        jButtonCoco.setBounds(200, 230, 140, 130);

        jLabelBatata.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelBatata.setText("Batatas");
        jPanel1.add(jLabelBatata);
        jLabelBatata.setBounds(230, 20, 70, 22);

        jLabelSalada.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelSalada.setText("Salada");
        jPanel1.add(jLabelSalada);
        jLabelSalada.setBounds(390, 20, 70, 20);

        jLabelAgua.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelAgua.setText("Agua");
        jPanel1.add(jLabelAgua);
        jLabelAgua.setBounds(80, 200, 50, 20);

        jLabelCoco.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelCoco.setText("Agua de Coco");
        jPanel1.add(jLabelCoco);
        jLabelCoco.setBounds(210, 200, 110, 20);

        jLabelCerveja.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelCerveja.setText("Cerveja");
        jPanel1.add(jLabelCerveja);
        jLabelCerveja.setBounds(390, 200, 60, 20);

        jLabelSanduiche.setFont(new java.awt.Font("Cantarell", 0, 17)); // NOI18N
        jLabelSanduiche.setText("Sanduiche");
        jPanel1.add(jLabelSanduiche);
        jLabelSanduiche.setBounds(60, 20, 90, 22);

        jTableListaPedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Produto", "Preco"
            }
        ));
        jScrollPane1.setViewportView(jTableListaPedido);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(522, 10, 230, 350);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 50, 770, 400);

        jLabelFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/background-clean.jpg"))); // NOI18N
        getContentPane().add(jLabelFundo);
        jLabelFundo.setBounds(-5, -2, 800, 450);

        setSize(new java.awt.Dimension(804, 497));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void carregarListaPedido() {
        ArrayList<Pedido> listaPedido = pedido.getListaPedido();
        DefaultTableModel modelo = (DefaultTableModel) jTableListaPedido.getModel();
        modelo.setRowCount(0);
        for (Pedido b : listaPedido) {
            modelo.addRow(new String[]{b.getProduto(), b.getPreco()});
        }
        jTableListaPedido.setModel(modelo);
    }
    private void jButtonSanduicheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSanduicheActionPerformed
     
    }//GEN-LAST:event_jButtonSanduicheActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FazerPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FazerPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FazerPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FazerPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FazerPedido().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgua;
    private javax.swing.JButton jButtonCerveja;
    private javax.swing.JButton jButtonCoco;
    private javax.swing.JButton jButtonFritas;
    private javax.swing.JButton jButtonSalada;
    private javax.swing.JButton jButtonSanduiche;
    private javax.swing.JLabel jLabelAgua;
    private javax.swing.JLabel jLabelBatata;
    private javax.swing.JLabel jLabelCerveja;
    private javax.swing.JLabel jLabelCoco;
    private javax.swing.JLabel jLabelFundo;
    private javax.swing.JLabel jLabelPedidos;
    private javax.swing.JLabel jLabelSalada;
    private javax.swing.JLabel jLabelSanduiche;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableListaPedido;
    // End of variables declaration//GEN-END:variables
}
