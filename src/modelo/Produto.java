package modelo;


public class Produto {
    
    private String nomeProduto;
    private String tipo;
    private String id;
    private String quantidade;
    private String preco;
    
    public Produto(){
    }

    /**
     *
     * @param id
     * @param nomeProduto
     * @param tipo
     * @param quantidade
     * @param preco
     */
    public Produto(String id, String nomeProduto, String tipo, String quantidade, String preco) {
        this.id = id;
        this.nomeProduto = nomeProduto;
        this.tipo = tipo;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    /**
     *
     * @param nomeProduto
     */
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }
    
    public String toString() {
        return "Id: "+ id +"Nome: "+ nomeProduto + "\n" +"Tipo: " + tipo + "\n" + "Preço: " +  preco + "\n"+ "Quantidade: " +  quantidade + "\n"; 
    }
    
}