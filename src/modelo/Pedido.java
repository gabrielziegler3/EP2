package modelo;


public class Pedido {
    
    private String produto;
    private String quantidade;
    private String preco;
    private String precoTotal;
    
    public Pedido(){
    }

    public Pedido(String produto, String quantidade, String preco) {
        this.produto = produto;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }
    
    public String getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(String precoTotal) {
        this.precoTotal = precoTotal;
    }

    public String toString () {
        return "Produto :" + produto + "\n" + "Quantidade: " + quantidade + "\n" + "Preco :" + preco + "\n" ;
    }

}